# liberandose de premier

Video tutorial software libre Kdenlive y ObsStudio

https://youtu.be/uByuAqgGB6o



## Name
Liberandose de premier 

## Description
Se realiza un video tutorial de una herrramienta libre de edición de video con las acciones y efectos básicos para el manejo de dicha herramienta usando software libre para realizar el proceso de grabación de pantalla. Estos softwares son: Kden live (para la edición de video) y OBS Studio (para la grabación de pantalla). Y mostrando fuentes donde es posible extraer elementos 100% libres para el desarrollo del tutorial (imágenes, fragmentos de videos, gif, audios) 




## Authors and acknowledgment
Juan Felipe Marin Nieto 



## Project status
Proyecto terminado, se recomienda realizar las grabaciones por clips y juntar todo no una sola grabacion.
